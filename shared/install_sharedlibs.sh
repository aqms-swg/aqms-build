#! /bin/csh -f


# Please make sure DEVROOT point to proper location before executing

ln -s $DEVROOT/lib/cms/libcms.so libcms.so
ln -s $DEVROOT/lib/eventsolution/libeventsolution.so libeventsolution.so
ln -s $DEVROOT/lib/gcda/libgcda.so libgcda.so
ln -s $DEVROOT/lib/hk/libhk.so libhk.so
ln -s $DEVROOT/lib/rtevent/librtevent.so librtevent.so
ln -s $DEVROOT/lib/rtpage/librtpage.so librtpage.so
ln -s $DEVROOT/lib/rtseis/librtseis.so librtseis.so
ln -s $DEVROOT/lib/tnalarm/libtnalarm.so libtnalarm.so
ln -s $DEVROOT/lib/tnchnl/libtnchnl.so libtnchnl.so
ln -s $DEVROOT/lib/tndb/libtndb.so libtndb.so
ln -s $DEVROOT/lib/tnframework/libtnframework.so libtnframework.so
ln -s $DEVROOT/lib/tnframework/libtnframework_nocms.so libtnframework_nocms.so
ln -s $DEVROOT/lib/tnpkg/libtnpkg.so libtnpkg.so
ln -s $DEVROOT/lib/tnstd/libtnstd.so libtnstd.so
ln -s $DEVROOT/lib/tntcp/libtntcp.so libtntcp.so
ln -s $DEVROOT/lib/tntime/libtntime.so libtntime.so
ln -s $DEVROOT/lib/tnwave/libtnwave.so libtnwave.so
ln -s $DEVROOT/lib/tnwaveclient/libtnwaveclient.so libtnwaveclient.so
ln -s $DEVROOT/lib/tnwavepool/libtnwavepool.so libtnwavepool.so
ln -s $DEVROOT/lib/ewlib/libew.a libew.a


