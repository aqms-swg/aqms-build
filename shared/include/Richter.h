/***********************************************************

File Name :
	rstruct.h

Programmer:
	Phil Maechling

Description:
	Realtime richter data structure definition

Creation Date:
	12 September 1996


Usage Notes:



Modification History:



**********************************************************/

#ifndef rtr_H
#define rtr_H

#include "seismic.h"

struct richter_station_data_type
{
  char station[MAX_CHARS_IN_STATION_STRING];
  char channel[MAX_CHARS_IN_CHANNEL_STRING];
  float latitude;
  float longitude;
  float peak_acceleration;
  float peak_velocity;
  float peak_displacement;
  float peak_wa_amp;
  int   clipped;
  float dist;
  float az;
  float o_amp;
  float c_amp;
  int   glitch;
  int   outlier; 
};
#endif
