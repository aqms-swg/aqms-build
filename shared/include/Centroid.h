/***********************************************************

File Name :
	cdt.h

Programmer:
	Phil Maechling

Description:
	Centroid Data Type

Creation Date:
	17 September 1996


Usage Notes:



Modification History:
	8 September 1997


**********************************************************/

#ifndef cdt_H
#define cdt_H
#include "Richter.h"

struct centroid_data_type
{
  struct timeval origin_time;
  int   data_type;
  int   clipped_data_used;
  int   determine_depth;
  int   number_of_raw_channels;
  int   number_of_valid_channels;
  float stdv;
  float latitude;
  float longitude;
  float depth;
  float magnitude;
  float rct;
  int   boundary_err;
  int   fit_err;
  struct richter_station_data_type chan_list[MAX_CHANNELS_IN_NETWORK];
};

#endif
