#! /bin/csh -f

# Please make sure DEVROOT point to proper location before executing

ln -s $DEVROOT/lib/cms/libcms.a libcms.a
ln -s $DEVROOT/lib/eventsolution/libeventsolution.a libeventsolution.a
ln -s $DEVROOT/lib/gcda/libgcda.a libgcda.a
ln -s $DEVROOT/lib/hk/libhk.a libhk.a
ln -s $DEVROOT/lib/rtevent/librtevent.a librtevent.a
ln -s $DEVROOT/lib/rtpage/librtpage.a librtpage.a
ln -s $DEVROOT/lib/rtseis/librtseis.a librtseis.a
ln -s $DEVROOT/lib/tnalarm/libtnalarm.a libtnalarm.a
ln -s $DEVROOT/lib/tnchnl/libtnchnl.a libtnchnl.a
ln -s $DEVROOT/lib/tndb/libtndb.a libtndb.a
ln -s $DEVROOT/lib/tnframework/libtnframework.a libtnframework.a
ln -s $DEVROOT/lib/tnframework/libtnframework_nocms.a libtnframework_nocms.a
ln -s $DEVROOT/lib/tnpkg/libtnpkg.a libtnpkg.a
ln -s $DEVROOT/lib/tnstd/libtnstd.a libtnstd.a
ln -s $DEVROOT/lib/tntcp/libtntcp.a libtntcp.a
ln -s $DEVROOT/lib/tntime/libtntime.a libtntime.a
ln -s $DEVROOT/lib/tnwave/libtnwave.a libtnwave.a
ln -s $DEVROOT/lib/tnwaveclient/libtnwaveclient.a libtnwaveclient.a
ln -s $DEVROOT/lib/tnwavepool/libtnwavepool.a libtnwavepool.a
ln -s $DEVROOT/lib/ewlib/libew.a libew.a


