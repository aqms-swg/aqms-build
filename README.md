This repo is used to build a docker images containing AQMS thirdparty software dependencies. Subsequently, it can be used to build one or more AQMS software repos, especially the C++ code.

## Pre-requisites  
  
Install docker on your host server  
  
## Building the image

The ``build_scripts`` folder contains the necessary files to build the docker images.
  
*  ``Dockerfile`` builds a base image which contains all the AQMS thirdparty dependencies minus the database dependencies.  
*  ``Dockerfile.oracle`` uses the base image in the previous step to build an image which includes the Oracle12c instant client.
*  ``Dockerfile.postgres`` uses the base image in the previous step to build an image which includes the Postgres14 client.
*  ``Makefile`` runs the docker commands to build images.
*  ``build_thirdparty_linux64_centos7.sh`` installs OS dependencies, downloads and compiles third party dependencies needed by AQMS compiled software.  
*  ``build_oracele.sh`` installs the Oracle12c instant client.  
*  ``build_postgres.sh`` installs the Postgresql14 client and libraries.


To build docker image for Oracle12c  
  
``cd build_scripts``  
``make -f Makefile base``  
``make -f Makefile oracle``  

To build docker image for Postgresql14  
  
``cd build_scripts``  
``make -f Makefile base``  
``make -f Makefile postgres``  

