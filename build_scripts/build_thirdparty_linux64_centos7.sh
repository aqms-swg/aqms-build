############
# 
############

# install OS dependencies
yum -y install wget make unzip which git subscription-manager compat-libcap1  \
    compat-libstdc++-33 gcc gcc-c++ gcc-gfortran   \
    glibc glibc-devel imake libaio libaio-devel libconfig libconfig-devel    \
    libgfortran libpcap libpcap-devel libstdc++ libstdc++-devel libtool-ltdl \
    perl-devel perl-Data-Dumper subversion curl-devel

# install jdk
yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel
mkdir /usr/java
cd /usr/java
ln -s /usr/lib/jvm/java-openjdk java-openjdk
ln -s java-openjdk latest

# install gradle, v7.5.1 (latest on Oct 31 2022), needed by aqms-cms
mkdir -p /opt/gradle
cd /opt/gradle
wget https://services.gradle.org/distributions/gradle-7.5.1-bin.zip
unzip -d /opt/gradle gradle-7.5.1-bin.zip
ln -s gradle-7.5.1 latest
cd /usr/bin
ln -s /opt/gradle/latest/bin/gradle gradle
rm -f /opt/gradle/gradle-7.5.1-bin.zip
#exit

  
# Create the build tree  
# Start by defining the BUILD_ROOT variable. 

mkdir -p /build/linux64
export BUILD_ROOT=/build/linux64 

# Create the directory structure.
# The 'shared' directory will hold the or 3rd-party dependency programs that are needed to build AQMS. Be sure to add the external symlink. 

mkdir -p $BUILD_ROOT/{aqms,earthworm,shared}
cd $BUILD_ROOT && ln -s shared external


###
# Build shared programs
###

# 
# install ProductClient.jar, required for compiling aqms-cms
# 
cd $BUILD_ROOT/shared
wget https://usgs.github.io/pdl/ProductClient.zip
unzip -d ProductClient ProductClient.zip
rm -f ProductClient.zip

#
# ACE-TAO
# 
# Download the OCI ACE+TAO distribution As of this writing, the current version is 2.2a. (NOTE: You must use OCI's redistribution of the ACE ORB. Other distributions will not work). 
cd $BUILD_ROOT/shared
#wget http://download.ociweb.com/TAO-2.2a/ACE+TAO-2.2a_with_latest_patches.tar.gz
wget --no-check-certificate https://download.objectcomputing.com/TAO-2.2a/ACE+TAO-2.2a_with_latest_patches.tar.gz

#cp /tmp/ACE+TAO-2.2a_with_latest_patches.tar.gz .

# Unpack the distribution. This will create the `ACE_wrappers` directory in $BUILD_ROOT/shared. 
tar -zxvf ACE+TAO-2.2a_with_latest_patches.tar.gz

# Define the necessary environment variables. 
export ACE_ROOT=$BUILD_ROOT/shared/ACE_wrappers
export TAO_ROOT=$ACE_ROOT/TAO
export PATH=$ACE_ROOT/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin
export LD_LIBRARY_PATH=$ACE_ROOT/lib

# Create the $ACE_ROOT/ace/config.h file. 
cat >$ACE_ROOT/ace/config.h <<EOF
#include "ace/config-linux.h"
EOF

# Create the $ACE_ROOT/include/makeinclude/platform_macros.GNU file.  
cat >$ACE_ROOT/include/makeinclude/platform_macros.GNU <<EOF
include \$(ACE_ROOT)/include/makeinclude/platform_linux.GNU
EOF

# To compile all the core tao and orbsvcs libraries (including its dependencies in ACE and ACEXML), delete the *.sln files and then run MPC. 
cd $TAO_ROOT
find . -name \*.sln -delete
$ACE_ROOT/bin/mwc.pl TAO_ACE.mwc -type gnuace

# Now build ACE+TAO. 
# There should be more than 230 files written to the $ACE_ROOT/lib directory. If you end up with only 88 or so, then you'll need to revisit the previous step. 
make buildbits=64 2>&1 | tee make.log 

# Change ownership to root. (We don't want this directory to change…)
cd $BUILD_ROOT/shared && ls && chown -R root:root ACE_wrappers  

rm -f $BUILD_ROOT/shared/ACE+TAO-2.2a_with_latest_patches.tar.gz

#
# Xerces-C++
#  
# Download and unpack the distribution. The current version is 3.2.2.  DO NOT change this version as AQMS is untested with higher versions!
XERCES_VERSION="3.2.2"
cd $BUILD_ROOT/shared
wget --no-check-certificate https://archive.apache.org/dist/xerces/c/3/sources/xerces-c-$XERCES_VERSION.tar.gz
tar -zxvf xerces-c-$XERCES_VERSION.tar.gz

# Configure. Be sure to set the `CFLAGS` and `CXXFLAGS` correctly. 
cd xerces-c-$XERCES_VERSION
./configure --prefix=${BUILD_ROOT}/shared/xerces-c-$XERCES_VERSION CFLAGS=-m64 CXXFLAGS=-m64 2>&1 | tee config.log

# Build and install 
make 2>&1 | tee make.log
make install 2>&1 | tee install.log  # <-- install in the source tree

# Create a generic xerces-c symlink (we'll need this later…)
cd $BUILD_ROOT/shared && ln -s xerces-c-$XERCES_VERSION xerces-c

# Change ownership to root. (We don't want this directory to change…)
cd $BUILD_ROOT/shared && chown -R root:root xerces-c-$XERCES_VERSION

rm -f $BUILD_ROOT/shared/xerces-c-$XERCES_VERSION.tar.gz

#
# OTL
#
# The Oracle, ODBC, and DB2 CLI Template Library (OTL) is a C++ library for database access, written by Sergei Kuchin. The OTL exists since 1996. It consists of a single header file. Besides Oracle, the OTL supports DB2 (natively), and various database systems now, directly or indirectly via ODBC.
# Create the OTL directory. The current version is 4.0.437 

cd $BUILD_ROOT/shared
mkdir OTL    # <-- This MUST be uppercase
ls -ltr /tmp
cp /tmp/otlv4_h2.zip .

# Download & uncompress the OTL header file 
#wget http://otl.sourceforge.net/otlv4_h2.zip
unzip -d $BUILD_ROOT/shared/OTL otlv4_h2.zip
cd $BUILD_ROOT/shared/OTL

# Change ownership to root. (We don't want this directory to change…)
cd $BUILD_ROOT/shared && chown -R root:root OTL


#
# qlib2
# 
# Download and uncompress the source. 
cd $BUILD_ROOT/shared
wget https://www.ncedc.org/qug/software/ucb/qlib2.2019.365.tar.gz
tar -zvxf qlib2.2019.365.tar.gz

#  There's no need to build the libraries now. They will get built during the Earthworm build. Build the 64-bit libraries. 
cd ${BUILD_ROOT}/shared/qlib2
cp -p Makefile Makefile.orig

# Set the installation directory to '/usr/local' (default is /share/apps), via sed
sed -i 's/ROOTDIR = \/share\/apps/#ROOTDIR = \/share\/apps/' Makefile
sed -i 's/LEAPSECONDS = $(ROOTDIR)\/etc\/leapseconds/#LEAPSECONDS = $(ROOTDIR)\/etc\/leapseconds/' Makefile

sed -i 's/#ROOTDIR = \/usr\/local/ROOTDIR = \/usr\/local/' Makefile
sed -i 's/#LEAPSECONDS = \/usr\/local\/etc\/leapseconds/LEAPSECONDS = \/usr\/local\/etc\/leapseconds/' Makefile
  
make clean 2>&1 | tee clean.log
make all64 2>&1 | tee make.log

rm -f $BUILD_ROOT/shared/qlib2.2019.365.tar.gz

#
# Comserv2
#
# Download latest development version from subversion repository. 
cd $BUILD_ROOT/shared
svn export svn://svn.isti.com/comserv2/dev comserv2

# build it
cd $BUILD_ROOT/shared/comserv2/src
export NUMBITS=64
make -f Makefile.linux clean 2>&1 | tee clean.log    # <-- optional
make -f Makefile.linux 2>&1 | tee make.log

#  Change ownership to root. (We don't want this directory to change…)
cd $BUILD_ROOT/shared && chown -R root:root comserv2


#
# Comserv3
# Instructions kindly provided by Mike B : https://scsnwiki.gps.caltech.edu/doku.php?id=blog:mlb_sw_comserv3
#
# Download and build libslink
cd $BUILD_ROOT/shared
git clone https://github.com/iris-edu/libslink.git
cd libslink
make clean
make -f Makefile 2>&1 | tee make.log
ln -s libslink.a libslink_64.a               # Don't forget to make the symlink...


# Download and build libdali
cd $BUILD_ROOT/shared
git clone https://github.com/iris-edu/libdali.git
cd libdali
make clean
make -f Makefile 2>&1 | tee make.log
ln -s libdali.a libdali_64.a                 # Don't forget to make the symlink...


# Download and build comserv3
cd $BUILD_ROOT/shared
git clone https://github.com/BerkeleySeismoLab/comserv3.git
cd comserv3
cp -p Makefile.include Makefile.include.ORIG

# Fix include files, not sure why this is needed...
cd include
cp -p dpstruc.h dpstruc.h.orig
sed -i '36d' dpstruc.h
sed -i '38 i #ifndef cs_dpstruc_h' dpstruc.h
sed -i '41 i #include "cstypes.h"' dpstruc.h
sed -i '42 i #endif' dpstruc.h
cat dpstruc.h

# Fix stuff.f
cp -p stuff.h stuff.h.orig
sed -i '27 i #ifndef cs_dpstruc_h' stuff.h
#sed -i '28 i #define cs_dpstruc_h' stuff.h
sed -i '30 i #endif' stuff.h
cat stuff.h

cd  ../
sed -i 's/QLIB2_DIR\s\{1,\}= \/data\/love\/doug\/git\/external_libs\/qlib2/QLIB2_DIR = \/build\/linux64\/shared\/qlib2/' Makefile.include
sed -i 's/SLINK_DIR = \/data\/love\/doug\/git\/external_libs\/libslink/SLINK_DIR = \/build\/linux64\/shared\/libslink/' Makefile.include
sed -i 's/DALI_DIR = \/data\/love\/doug\/git\/external_libs\/libdali-master/DALI_DIR = \/build\/linux64\/shared\/libdali/' Makefile.include

cd comserv3

# Set the NUMBITS variable
export NUMBITS=64

# Clean up the old stuff
make clean
rm -rf  bin.64    # if necessary

# Build & install
make 2>&1 | tee make.log
make install 2>&1 | tee install.log    # files are left in the bin.64 directory

# comserv
cd comserv_src
make clean
make 2>&1 | tee make.log
cp -p comserv ../bin.64


# cpick_card_server
cd ../clients.ucb/cpick_card_server
make clean
make 2>&1 | tee make.log
cp -p cpick_card_server ../../bin.64

# cs2ringserver
cd ../../clients.ucb/cs2ringserver
make clean
make 2>&1 | tee make.log
cp -p cs2ringserver ../../bin.64

# evtalarm
cd ../../clients.ucb/evtalarm
make clean
make 2>&1 | tee make.log
cp -p evtalarm  ../../bin.64


##
## Oracle instant client 12.2.0.1.0
## Available at https://gitlab.com/aqms-swg/aqms-thirdparty/-/tree/master/oracle-instantclient
## 
## Download the client zips
##cd $BUILD_ROOT/shared
##wget -O https://gitlab.com/aqms-swg/aqms-thirdparty/-/raw/master/oracle-12c-instantclient.tar.gz
##tar -zvxf oracle-12c-instantclient.tar.gz
##ls aqms-thirdparty-master-oracle-instantclient/oracle-instantclient
#
## Create installation directory & 'current' symlink 
#mkdir -p /app/oracle/product/12.2.0
#cd /app/oracle/product && ln -s 12.2.0 current 
#cd /app/oracle/product/current
#
#wget https://gitlab.com/aqms-swg/aqms-thirdparty/-/raw/master/oracle-12c-instantclient.tar.gz
#tar -zvxf oracle-12c-instantclient.tar.gz
#unzip 'instantclient-*-linux.x64-12.2.0.1.0.zip'
#mv instantclient_12_2 client64
#
# clean up
#rm -f *.tar.gz *.zip

##Create generic libclntsh.so symlink
#cd /app/oracle/product/current/client64
#ln -s libclntsh.so.12.1 libclntsh.so


####
# Earthworm
####
# Download and unpack the latest version (currently v7.9):
cd $BUILD_ROOT/earthworm
wget http://www.earthwormcentral.org/distribution/older_releases/earthworm_7.9-src.tar.gz
tar -zxvf earthworm_7.9-src.tar.gz

# Since the build and installation directories are different, I like to create a separate environment file for builds (ex, ew_linux_build.bash). First, define the EW_HOME, EW_VERSION and EWBITS variables:
cd $BUILD_ROOT/earthworm/earthworm_7.9/environment
cat >ew_linux_build64.bash <<EOF
export EW_HOME=${BUILD_ROOT}/earthworm
export EW_VERSION=earthworm_7.9
export EWBITS=64
EOF

# Now, extract everything under 'EWBITS=32' in the ew_linux.bash into your 'build' file:
sed -e '1,/EWBITS=32/d' ew_linux.bash >> ew_linux_build64.bash

#and then change the '-m${EWBITS}' option to '-fPIC' in the GLOBALFLAGS definition. Be sure not to change the FFLAGS definition:
sed -i -e '/GLOBALFLAGS/s/-m${EWBITS}/-fPIC/' ew_linux_build64.bash

#Now, while you're here, source the 'build' script:
source ew_linux_build64.bash

#Although EW comes with its own version of qlib2, we want to use the 'shared' version instead. In that way, everything is using the same thing (for better or for worse…).
cd $BUILD_ROOT/earthworm/earthworm_7.9/src/libsrc
mv qlib2  qlib2.orig
ln -s $BUILD_ROOT/shared/qlib2 qlib2

# Clean-up the source directory
cd $BUILD_ROOT/earthworm/earthworm_7.9/src
make clean_solaris; make clean_bin_solaris
make clean_unix; make clean_bin_unix
rm ../bin/*.{bat,cmd,exe,EXE,DLL}

#Now, build Earthworm
make unix 2>&1 | tee make.log
ln -s $BUILD_ROOT/earthworm/earthworm_7.9 $BUILD_ROOT/earthworm/shared

rm -f $BUILD_ROOT/earthworm/earthworm_7.9-src.tar.gz

#
# geojson2ew modules
#
#  The version of geojson2ew that comes with Earthworm v7.9 does not work (correctly), so it needs to be updated (v1.0.6) via svn and added to the distribution.
# Enable the 'optional-rpms' repo, and install the dependencies. 
subscription-manager repos --enable=rhel-7-server-optional-rpms
yum clean all
yum -y install jansson-devel librabbitmq-devel    # <-- requires librabbitmq >= 0.8

# Rename the existing geojson2ew installation directory.
cd $BUILD_ROOT/earthworm/earthworm_7.9/src/data_sources
mv geojson2ew geojson2ew.orig

#Get the last changed revision ('info') from the EW subversion repo, and then checkout it out.
svn info svn://svn.isti.com/earthworm/trunk/src/data_sources/geojson2ew
svn co -r6962 svn://svn.isti.com/earthworm/trunk/src/data_sources/geojson2ew geojson2ew_svn6962

#Build the new geojson2ew
cd $BUILD_ROOT/earthworm/earthworm_7.9/src/data_sources/geojson2ew_svn6962
make -f makefile.unix clean
make -f makefile.unix 2>&1 | tee make.log

#Create symlink
cd $BUILD_ROOT/earthworm/earthworm_7.9/bin
mv geojson2ew geojson2ew_svn6962
ln -s geojson2ew_svn6962 geojson2ew

# change owner for aqms folder to aqms
cd $BUILD_ROOT && chown -R aqms:aqms aqms

# no more yum installs, yum clean all
yum clean all

#
# libexpat (used by ms_to_v0)
#
# download and build

##export CDMG_HOME=${BUILD_ROOT}/external/ms_to_v0/build
##export INC_EXPAT=${CDMG_HOME}/include/expat
##export LIB_EXPAT=${CDMG_HOME}/lib/expat

##export PUT_INC=${CDMG_HOME}/include/dataconvert
##export PUT_LIB=${CDMG_HOME}/lib/dataconvert

##mkdir -p ${CDMG_HOME} ${INC_EXPAT} ${LIB_EXPAT} ${PUT_INC} ${PUT_LIB} 

cd ${BUILD_ROOT}/shared/
wget https://gitlab.com/aqms-swg/aqms-thirdparty/-/raw/master/expat.zip
unzip expat.zip

cd expat
make clean
make -f Makefile

#copy include files to $INC_EXPAT
##cp -p xmlparse/*.h xmltok/*.h $INC_EXPAT

# create the library file
ar rcs expat.a ./xmlparse/*.o ./xmltok/*.o ./xmlwf/*.o
##cp expat.a $LIB_EXPAT/libexpat.a
##cd $LIB_EXPAT
##ranlib libexpat.a

##ls ${INC_EXPAT}
##ls ${LIB_EXPAT}

rm -f ${BUILD_ROOT}/shared/expat.zip








