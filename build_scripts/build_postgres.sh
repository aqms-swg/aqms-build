# install ecpg, needed by dbselect
yum -y install centos-release-scl epel-release

# install Postgres14 dependencies
yum -y module disable postgresql
yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
yum -y install postgresql14-devel

# these two installs are needed for compiling aqms with postgres
yum -y install postgresql14-odbc unixODBC-devel
yum clean all

cd /usr/bin
ln -s /usr/pgsql-14/bin/ecpg ecpg

# install Postgres14 dependencies
#yum -y module disable postgresql
#yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
#yum install postgresql14-odbc
#yum -y install unixODBC-devel



