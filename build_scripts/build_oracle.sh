##
## Oracle instant client 12.2.0.1.0
## Available at https://gitlab.com/aqms-swg/aqms-thirdparty/-/tree/master/oracle-instantclient
## 
## Download the client zips
##cd $BUILD_ROOT/shared
##wget -O https://gitlab.com/aqms-swg/aqms-thirdparty/-/raw/master/oracle-12c-instantclient.tar.gz
##tar -zvxf oracle-12c-instantclient.tar.gz
##ls aqms-thirdparty-master-oracle-instantclient/oracle-instantclient
#
# Create installation directory & 'current' symlink 
mkdir -p /app/oracle/product/12.2.0
cd /app/oracle/product && ln -s 12.2.0 current 
cd /app/oracle/product/current

wget https://gitlab.com/aqms-swg/aqms-thirdparty/-/raw/master/oracle-12c-instantclient.tar.gz
tar -zvxf oracle-12c-instantclient.tar.gz
unzip 'instantclient-*-linux.x64-12.2.0.1.0.zip'
mv instantclient_12_2 client64

# clean up
rm -f *.tar.gz *.zip

#Create generic libclntsh.so symlink
cd /app/oracle/product/current/client64
ln -s libclntsh.so.12.1 libclntsh.so